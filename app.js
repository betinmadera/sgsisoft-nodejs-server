/**
 * Created by andresdavid on 18/11/14.
 */
'use strict'

var http = require('http');
var server = http.createServer().listen(8004);
var io = require('socket.io').listen(server);

var cookie_reader = require('cookie');
var querystring = require('querystring');
var redis = require('redis');


console.log('Server running at http://127.0.0.1:8004/');

io.use(function(socket, next) {
    var handshake = socket.request;

    if (!handshake) {
        return next(new Error('[[error:not-authorized]]'));
    }
    next();
});

io.sockets.on('connection', function (socket) {
    console.log('\ngot a new connection from: ' + socket.id + '\n');

    var string =   socket.handshake.headers.cookie.replace(/; /gi, '&');
    var result = querystring.parse(string);
    var sessionId= result.sessionid;

    // Create redis client
    var client = redis.createClient();

    // Subscribe to the Redis events channel
    client.subscribe('notifications.' + sessionId);

    // Grab message from Redis and send to client
    client.on('message', function(channel, message){
        console.log('on message', message);
        socket.send(message);
    });

    // Unsubscribe after a disconnect event
    socket.on('disconnect', function () {
        client.unsubscribe('notifications.' + sessionId);
    });
});
