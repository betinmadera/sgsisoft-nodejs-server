/**
 * Created by andresdavid on 19/11/14.
 */

'use strict'


var http = require('http');
var server = http.createServer().listen(8002);
var io = require('socket.io').listen(server);
var cookie_reader = require('cookie');
var querystring = require('querystring');
var redis = require('redis');

console.log('Server running at http://127.0.0.1:8002/');


//Configure socket.io to store cookie set by Django
/*io.configure(function(){
    io.set('authorization', function(data, accept){
        if(data.headers.cookie){
            data.cookie = cookie_reader.parse(data.headers.cookie);
            return accept(null, true);
        }
        return accept('error', false);
    });
    io.set('log level', 1);
});*/

io.use(function(socket, next) {
    var handshake = socket.request;

    if (!handshake) {
        return next(new Error('[[error:not-authorized]]'));
    }
    next();
    /*cookieParser(handshake, {}, function(err) {
        if (err) {
            return next(err);
        }

        var sessionID = handshake.signedCookies['express.sid'];

        db.sessionStore.get(sessionID, function(err, sessionData) {
            if (err) {
                return next(err);
            }
            console.log(sessionData);

            next();
        });
    });*/
});

io.sockets.on('connection', function (socket) {
    console.log('\ngot a new connection from: ' + socket.id + '\n');

    // Create redis client
    var client = redis.createClient();

    console.log('notifications.' + socket.handshake.cookie['sessionid']);
    console.log(client);

    // Subscribe to the Redis events channel
    client.subscribe('notifications.' + socket.handshake.cookie['sessionid']);
    console.log('notifications.' + socket.handshake.cookie['sessionid']);
    console.log(client);

    // Grab message from Redis and send to client
    client.on('message', function(channel, message){
        console.log('on message', message);
        socket.send(message);
    });

    // Unsubscribe after a disconnect event
    socket.on('disconnect', function () {
        client.unsubscribe('notifications.' + socket.handshake.cookie['sessionid']);
    });
});


//var io= require('socket.io').listen(8008);

/*io.sockets.on("connection", function (socket) {
    console.log('Client Connected from ' +  socket.id   );
    socket.on('nuevo click', function(data){
        var values= querystring.stringify(data);
        io.sockets.emit("click new", values);
    });
});*/